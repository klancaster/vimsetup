set autoindent
runtime! macros/matchit.vim
set hidden
set cmdheight=2 "command bar is 2 high
set backspace=indent,eol,start "set backspace function
set hlsearch "highlight searched things
set incsearch "incremental search
set ignorecase "ignore case
set smartcase
set scrolloff=3
set textwidth=0
set autoread "auto read when file is changed from outside
set ruler "show current position
" set nu "show line number
set showmatch "show maching braces
set shiftwidth=2
set tabstop=2
set grepprg=ack
set grepformat=%f:%l:%m
set backspace=start,indent
set expandtab
:colorscheme slate
set nocompatible
set nu
let mapleader=","
filetype on
filetype plugin indent on
augroup filetypedetect
au! BufNewFile,BufRead *.ch setf cheat
au BufNewFile,BufRead *.liquid setf liquid
au! BufRead,BufNewFile *.html.haml  setfiletype haml
au! BufRead,BufNewFile *.haml setfiletype haml
autocmd BufNewFile,BufRead *.yml setf eruby
augroup END

autocmd BufNewFile,BufRead *_spec.rb source ~/.vim/ftplugin/rails/rspec.vim
autocmd BufNewFile,BufRead *_test.rb source ~/.vim/ftplugin/rails/shoulda.vim
augroup mkd
     autocmd BufRead *.mkd  set ai formatoptions=tcroqn2 comments=n:>
augroup END
set t_Co=256
imap <C-l> <space>=><space>
map <Leader>t :FuzzyFinderTextMate<CR>
nnoremap ' `
nnoremap ` '
set history=1000
nnoremap <C-e> <C-e><C-e><C-e>
nnoremap <C-y> <C-y><C-y><C-y>
set wildmenu
"
" latex
let g:tex_flavor='latex'
set nobackup
set nowritebackup
set noswapfile
set noerrorbells
set visualbell

